**Логи при обычном запуске и закрытии приложения:**

_->запуск_  
onCreate  
onStart  
onResume  
_->закрытие_  
onPause  
onStop  
onDestroy

**Логи, при повороте экрана:**

_->запуск_  
onCreate  
onStart  
onResume  
_->поворот_  
onPause  
onStop  
onDestroy  
onCreate  
onStart  
onResume  

При повороте экрана активити нужно снова отрисоваться, поэтому оно полностью завершается и начинает свой цикл заново с отрисовки

**Логи, при сворачивании:**  
_->запуск_  
onCreate  
onStart  
onResume  
_->сворачиваю_  
onPause  
onStop  

onPause - поверх появляется новое активити
onStop - активити уже не виден

_->разворачиваю_  
onStart  
onResume

**Логи, при finish() в onCreate:**  
onCreate  
onDestroy

Как только попадаем в onCreate, активити завершается с помощью finish()
